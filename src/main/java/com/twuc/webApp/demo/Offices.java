package com.twuc.webApp.demo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Offices {
    @Id
    private int id;
    @Column
    private String city;

    public Offices() {
    }

    public Offices(int id, String city) {
        this.id = id;
        this.city = city;
    }

    public int getId() {
        return id;
    }

    public String getCity() {
        return city;
    }
}
