package com.twuc.webApp.demo;

import com.twuc.webApp.demo.Offices;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OfficesRepository extends JpaRepository<Offices, Long> {

}
