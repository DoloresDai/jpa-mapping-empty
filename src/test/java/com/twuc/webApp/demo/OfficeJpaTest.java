package com.twuc.webApp.demo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("SpringTestingDirtiesContextInspection")
@DataJpaTest(showSql = false)
@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
class OfficeJpaTest {
    @Autowired
    private OfficesRepository repo;
    @Autowired
    private EntityManager em;

    @Test
    void hello_world() {
        Offices offices = repo.save(new Offices(1, "WU"));
        Offices offices2 = repo.save(new Offices(2, "XA"));
        repo.flush();

        assertNotNull(offices);
    }

    @Test
    void should_persist_entity() {
        em.persist(new Offices(3,"CD"));
        em.flush();
        em.clear();
        Offices offices = em.find(Offices.class, 3);

        assertEquals(3,offices.getId());

    }
}
